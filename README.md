# About this repo #

* This is the repository for the Quantified self project Web Application done solely by Seyedehzahra Jalili
* Version 0.1

#### What is new? ####

* 1) The backend for two plot pages are developed. 
* 2) The Glucose plot page uses bokeh and Ajax to update the plots without refreshing the whole page. 
* 3) The Blood Pressure also used bokeh and Ajax to show the Systolic and Diastolic blood pressure for the female participant.
* 4) In this project we used csv files for the datasets instead of previous pymysql

* 5) Note that all the pages are designed and developed by Zahra Jalili in this version and no code is used by the previous developers.


#### How do I get set up? ####

 First clone the master branch, secondly create a separate branch.
 
#### Dependencies ####
* Only use Bokeh version 1.4.0 for this app

 * The Flask Framework module
 * The Bokeh 1.4.0 module for visualization in web apps
 * The Pandas module for data structure manipulation
 

#### How to install the modules
  
* `$ python pip3 install flask --upgrade`
* `$ python pip3 install bokeh==1.4.0 --upgrade`
* `$ python pip3 install pandas --upgrade`

## How to run the application ##

#### On a Linux environment ####
On the commandline run:
```
$ export FLASK_APP=webapp.py
$ python -m flask run
```

#### On a Windows environment ####
In command prompt run:
```
C:\path\to\app>set FLASK_APP=webapp.py
C:\path\to\app>python -m flask run
```


#### Who do I talk to? ####

* Any pressing questions reach out to admin [s.jalili@st.hanze.nl]

![Hanze Logo](https://www.hollandisc.com/-/media/ISC/Holland%20ISC/Images/Hanze%20logo%20160%20x%20400.jpg)