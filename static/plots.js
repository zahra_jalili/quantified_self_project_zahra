window.onload = function () {
   $(':checkbox').prop('checked', true);
};

function intefy(bool) {
    return bool ? 1 : 0;
}

function get_vals(opts) {
    var vals = [];
    for (i = 0; i < opts.length; i++) {
        if ($(opts[i]).prop("checked")) {
            vals.push($(opts[i]).prop("value"));
        }
    }
    return vals;
}

$(document).ready(function() {

    var el = document.getElementById("bloodpressure");
    el.classList.remove('plot-spacer');
    el.style.height = "0px";

    $("a.makeplot").click(function(event) {
        event.preventDefault();
        var container = $(this).closest('.container');
        var plot_form = $(container).find("form");
        var plot_options = $(plot_form).find("div.options").find("input.option");
        var options = get_vals(plot_options);
        var plot = $(container).attr("class").split(/\s+/)[1];
        var plot_space = plot.concat('_plot');
        var data = {
            "plot" : plot,
            "par" : [
                intefy($(plot_form.find('input.par1')).prop('checked')),
                intefy($(plot_form.find('input.par2')).prop('checked'))
            ],
            "pha" : [
                intefy($(plot_form.find('input.pha1')).prop('checked')),
                intefy($(plot_form.find('input.pha2')).prop('checked')),
                intefy($(plot_form.find('input.pha3')).prop('checked'))
            ],
            "options" : options,
            "height" : $("#".concat(plot_space)).innerHeight(),
            "width" : $("#".concat(plot_space)).innerWidth()
        };

        $.ajax({
                type: 'POST',
                url: "/plot/get_plot",
                data: JSON.stringify(data, null, "\t"),
                contentType: 'application/json;charset=UTF-8',
                success: function(response){
                    $("#".concat(plot_space)).html("");
                    Bokeh.embed.embed_item(JSON.parse(response), plot_space)
                }
            });
    });

    $('.participants').click(function (event) {
        var other = ($(this).attr("class").split(/\s+/)[1] == "par1") ? "par2" : "par1";
        var plot_form = $(this).closest("form");
        var other_cb = $(plot_form.find('input.'.concat(other)));
        if (!(other_cb.prop("checked"))) {
            other_cb.prop("checked", true)
        }
    });

    window.onwheel = beforeScroll;
    window.onkeydown = beforeScroll;
    var scrollVal = 0;
    function beforeScroll(e) {
        if (e.type == 'keydown' && (e.key == 'ArrowDown' || e.key == 'ArrowUp')) {
            if (e.key == 'ArrowDown')
                scrollDown();
            else if (e.key == 'ArrowUp')
                scrollUp();
        }
        e.preventDefault();
        return;
    }

    function scrollUp() {
        if (scrollVal > 0)
            scrollVal -= (window.innerHeight * 1.004);

        this.scroll(0, scrollVal);
    }

    function scrollDown() {
        if (scrollVal < document.body.scrollHeight)
            scrollVal += (window.innerHeight * 1.004);
        //should detect for maximum scroll
        this.scroll(0, scrollVal);
    }
});

