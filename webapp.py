#!/usr/bin/python3

"""
    Copyright (C) 2019  Zahra Jalili

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from flask import Flask, render_template, request, redirect, abort, send_from_directory, jsonify
import argparse
import os.path

import logging

from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from bokeh.models.sources import AjaxDataSource
import pandas as pd


""" loading csv file to pandas dataframe """
female_glucose = pd.read_csv('data/female_glucose.csv')

""" selecting specific columns """
female_glucose = female_glucose[['Device Timestamp', 'Historic Glucose mmol/L']]

""" renaming columns """
female_glucose.rename(columns={"Device Timestamp": "Timestamp", "Historic Glucose mmol/L": "Glucose"}, inplace=True)

""" converting timestamp column to datatime type """
female_glucose['Timestamp'] = pd.to_datetime(female_glucose['Timestamp'], format='%d-%m-%Y %H:%M')

""" converting glucose column to float """
female_glucose['Glucose'] = female_glucose['Glucose'].astype(dtype='float')

""" removing NaN rows from dataframe """
female_glucose.dropna(inplace=True)

""" first base line data"""
mask = (female_glucose['Timestamp'] > '18-10-2019 00:00:00') & (female_glucose['Timestamp'] <= '26-10-2019 23:59:59')
female_glucose_first_base_line = female_glucose.loc[mask]

""" fasting period data"""
mask = (female_glucose['Timestamp'] > '27-10-2019 00:00:00') & (female_glucose['Timestamp'] <= '23-11-2019 23:59:59')
female_glucose_fasting_period = female_glucose.loc[mask]

""" group the data by the day time """
times = pd.to_datetime(female_glucose_first_base_line['Timestamp'])
female_glucose_first_base_line = female_glucose_first_base_line.groupby([times.dt.hour]).mean()
female_glucose_first_base_line_list = female_glucose_first_base_line['Glucose'].tolist()

times = pd.to_datetime(female_glucose_fasting_period['Timestamp'])
female_glucose_fasting_period = female_glucose_fasting_period.groupby([times.dt.hour]).mean()
female_glucose_fasting_period_list = female_glucose_fasting_period['Glucose'].tolist()

""" loading csv file to pandas dataframe """
male_glucose = pd.read_csv('Data/male_glucose.csv')

""" selecting specific columns """
male_glucose = male_glucose[['Device Timestamp', 'Historic Glucose mmol/L']]

""" renaming columns """
male_glucose.rename(columns={"Device Timestamp": "Timestamp", "Historic Glucose mmol/L": "Glucose"}, inplace=True)

""" converting timestamp column to datatime type """
male_glucose['Timestamp'] = pd.to_datetime(male_glucose['Timestamp'], format='%d-%m-%Y %H:%M')

""" converting glucose column to float """
male_glucose['Glucose'] = male_glucose['Glucose'].astype(dtype='float')

""" removing NaN rows from dataframe """
male_glucose.dropna(inplace=True)

""" first base line data"""
mask = (male_glucose['Timestamp'] > '18-10-2019 00:00:00') & (male_glucose['Timestamp'] <= '26-10-2019 23:59:59')
male_glucose_first_base_line = male_glucose.loc[mask]

""" fasting period data"""
mask = (male_glucose['Timestamp'] > '27-10-2019 00:00:00') & (male_glucose['Timestamp'] <= '23-11-2019 23:59:59')
male_glucose_fasting_period = male_glucose.loc[mask]

""" group the data by the day time """
times = pd.to_datetime(male_glucose_first_base_line['Timestamp'])
male_glucose_first_base_line = male_glucose_first_base_line.groupby([times.dt.hour]).mean()
male_glucose_first_base_line_list = male_glucose_first_base_line['Glucose'].tolist()

times = pd.to_datetime(male_glucose_fasting_period['Timestamp'])
male_glucose_fasting_period = male_glucose_fasting_period.groupby([times.dt.hour]).mean()
male_glucose_fasting_period_list = male_glucose_fasting_period['Glucose'].tolist()

""" loading csv file to pandas dataframe """
female_BP = pd.read_csv('data/female_BP.csv')

""" selecting specific columns """
female_BP = female_BP[['Measurement Date', 'SYS', 'DIA']]

""" renaming columns """
female_BP.rename(columns={"Measurement Date": "Timestamp"}, inplace=True)

""" converting timestamp column to datatime type """
female_BP['Timestamp'] = pd.to_datetime(female_BP['Timestamp'], format='%Y-%m-%d %H:%M')

""" converting SIS and DIA columns to float """
female_BP['SYS'] = female_BP['SYS'].astype(dtype='float')
female_BP['DIA'] = female_BP['DIA'].astype(dtype='float')
""" removing NaN rows from dataframe """
female_BP.dropna(inplace=True)

""" first base line data"""
mask = (female_BP['Timestamp'] > '18-10-2019 00:00:00') & (female_BP['Timestamp'] <= '26-10-2019 23:59:59')
female_BP_first_base_line = female_BP.loc[mask]

""" fasting period data"""
mask = (female_BP['Timestamp'] > '27-10-2019 00:00:00') & (female_BP['Timestamp'] <= '23-11-2019 23:59:59')
female_BP_fasting_period = female_BP.loc[mask]

""" group the data by the day time """
times = pd.to_datetime(female_BP_first_base_line['Timestamp'])
female_BP_first_base_line = female_BP_first_base_line.groupby([times.dt.hour]).mean()
female_BP_first_base_line_SYS_list = female_BP_first_base_line['SYS'].tolist()
female_BP_first_base_line_DIA_list = female_BP_first_base_line['DIA'].tolist()

times = pd.to_datetime(female_BP_fasting_period['Timestamp'])
female_BP_fasting_period = female_BP_fasting_period.groupby([times.dt.hour]).mean()
female_BP_fasting_period_SYS_list = female_BP_fasting_period['SYS'].tolist()
female_BP_fasting_period_DIA_list = female_BP_fasting_period['DIA'].tolist()
hours_BP_first_baseline = [0, 4, 8, 9, 10, 12, 19, 21, 22, 23]
hours_BP_fasting_period = [0, 1, 7, 8, 9, 10, 11, 15, 19, 20, 21, 22, 23]

# Config and globals
app = Flask(__name__)
local = False
cfg_loc = os.path.join(os.getcwd(), 'db.yaml')


@app.route('/')
def home():
    """
    specifies the path to the homepage of the web application
    :return: rendered html template to the client
    """
    return render_template("newHome.html", text='homepaage!')


@app.route('/data')
def article():
    """
    specifies the path to the test page of the web application
    :return: rendered html template to the client
    """
    return render_template("newHome.html")


@app.route('/downloadArticle', methods=['GET', 'POST'])
def download():
    return send_from_directory(directory=os.getcwd() + "/static/pdfs", filename="paper.pdf", mimetype='application/pdf')


@app.route('/team')
def team_page():
    """
    specifies the path to the page of the team names and information web application
    :return: rendered html template to the client
    """
    return render_template("newTeam.html")


@app.route('/contact')
def contact_page():
    """
    specifies the path to the contact form of the web application
    :return: rendered html template to the client
    """
    return render_template("newContact.html", text="Contact, ...")


x = -1


@app.route('/data_female_baseline/', methods=['POST'])
def data_female_baseline():
    global x
    if (x >= 24):
        return "nothing"
    if (x == -1):
        x += 1
        return "nothing"
    y = female_glucose_first_base_line_list[x]
    x += 1
    return jsonify(x=[x], y=[y])


ii = -1


@app.route('/data_female_fasting/', methods=['POST'])
def data_female_fasting():
    global ii
    if (ii >= 24):
        return "nothing"
    if (ii == -1):
        ii += 1
        return "nothing"
    y = female_glucose_fasting_period_list[ii]
    ii += 1
    return jsonify(x=[ii], y=[y])


x3 = -1


@app.route('/data_female_BP_baseline/', methods=['POST'])
def data_female_BP_baseline():
    global x3
    if (x3 >= len(hours_BP_first_baseline)):
        return "nothing"
    if (x3 == -1):
        x3 += 1
        return "nothing"
    y = female_BP_first_base_line_SYS_list[x3]
    z = female_BP_first_base_line_DIA_list[x3]
    xx = hours_BP_first_baseline[x3]
    x3 += 1
    return jsonify(x=[xx], y=[y], w=[xx], z=[z])


ii3 = -1


@app.route('/data_female_BP_fasting/', methods=['POST'])
def data_female_BP_fasting():
    global ii3
    if (ii3 >= len(hours_BP_fasting_period)):
        return "nothing"
    if (ii3 == -1):
        ii3 += 1
        return "nothing"
    y = female_BP_fasting_period_SYS_list[ii3]
    z = female_BP_fasting_period_DIA_list[ii3]
    xx = hours_BP_fasting_period[ii3]
    ii3 += 1
    return jsonify(x=[xx], y=[y], w=[xx], z=[z])


x2 = -1


@app.route('/data_male_baseline/', methods=['POST'])
def data_male_baseline():
    global x2
    if (x2 >= 24):
        return "nothing"
    if (x2 == -1):
        x2 += 1
        return "nothing"
    y = male_glucose_first_base_line_list[x2]
    x2 += 1
    return jsonify(x=[x2], y=[y])


ii2 = -1


@app.route('/data_male_fasting/', methods=['POST'])
def data_male_fasting():
    global ii2
    if (ii2 >= 24):
        return "nothing"
    if (ii2 == -1):
        ii2 += 1
        return "nothing"
    y = male_glucose_fasting_period_list[ii2]
    ii2 += 1
    return jsonify(x=[ii2], y=[y])


@app.route('/glucose_plot/')
def show_glucose_plot():
    global x, ii, x2, ii2
    x = -1
    ii = -1
    x2 = -1
    ii2 = -1
    plots = []
    plots.append(make_ajax_plot_female_baseline())
    plots.append(make_ajax_plot_female_fasting())
    # plots.append(make_plot())
    plots2 = []
    plots2.append(make_ajax_plot_male_baseline())
    plots2.append(make_ajax_plot_male_fasting())

    names_list = ['Female\'s Average Glucose Level 24 Hours of a Day for Base-line Period',
                  'Female\'s Average Glucose Level 24 Hours of a Day for Fasting Period']
    names_list2 = ['Male\'s Average Glucose Level 24 Hours of a Day for Base-line Period',
                   'Male\'s Average Glucose Level 24 Hours of a Day for Fasting Period']
    return render_template('dashboard.html', len=len(plots), plots=plots, names_list=names_list, plots2=plots2,
                           names_list2=names_list2)


@app.route('/BP_plot/')
def show_BP_plot():
    global x3, ii3
    x3 = -1
    ii3 = -1
    plots = []
    plots.append(make_ajax_BP_plot_female_baseline())
    plots.append(make_ajax_BP_plot_female_fasting())

    names_list = ['Female\'s Average Blood Pressure 24 Hours of a Day for Base-line Period',
                  'Female\'s Average Blood Pressure 24 Hours of a Day for Fasting Period']
    return render_template('dashboard2.html', len=len(plots), plots=plots, names_list=names_list)




def make_ajax_plot_female_baseline():
    source = AjaxDataSource(data_url=request.url_root + 'data_female_baseline/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="blue")

    script, div = components(plot)
    return script, div


def make_ajax_BP_plot_female_baseline():
    source = AjaxDataSource(data_url=request.url_root + 'data_female_BP_baseline/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[], w=[], z=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="blue", legend_label='Systolic')
    plot.line('w', 'z', source=source, line_width=4, line_color="green", legend_label="Diastolic")

    script, div = components(plot)
    return script, div


def make_ajax_BP_plot_female_fasting():
    source = AjaxDataSource(data_url=request.url_root + 'data_female_BP_fasting/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[], w=[], z=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="red", legend_label='Systolic')
    plot.line('w', 'z', source=source, line_width=4, line_color="green", legend_label='Diastolic')

    script, div = components(plot)
    return script, div


def make_ajax_plot_male_baseline():
    source = AjaxDataSource(data_url=request.url_root + 'data_male_baseline/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="blue")

    script, div = components(plot)
    return script, div


def make_ajax_plot_female_fasting():
    source = AjaxDataSource(data_url=request.url_root + 'data_female_fasting/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="red")

    script, div = components(plot)
    return script, div


def make_ajax_plot_male_fasting():
    source = AjaxDataSource(data_url=request.url_root + 'data_male_fasting/',
                            polling_interval=1200, mode='append')

    source.data = dict(x=[], y=[])

    plot = figure(plot_height=200, sizing_mode='scale_width', x_axis_label='Time of Day')
    plot.line('x', 'y', source=source, line_width=4, line_color="red")

    script, div = components(plot)
    return script, div




@app.route('/hypothesis')
def hypothesis():
    return render_template("hypothesis.html")


@app.route('/hypothesis/<string:part>')
def jump_to_hypothesis(part):
    hypotheses = ["bloodpressure", "cognition", "glucose", "heartrate", "sleepsummary", "weight"]
    if part in hypotheses:
        return redirect('/hypothesis#' + part)
    else:
        abort(404)


@app.errorhandler(404)
def page_not_found(error):
    error = error.__str__().replace('404 Not Found: ', '')
    return render_template('404.html', error=error), 404


def set_local():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--remote', help='make a remote connection to the db', action='store_true',
                        required=False)
    args = vars(parser.parse_args())
    global local
    local = args['remote'] is False


@app.route('/hypos/glucose')
def glucose_page():
    return render_template("hypo_glucose.html", text="Glucose")


@app.route('/hypos/cognition')
def cognition_page():
    return render_template("hypo_cog.html", text="Cognition")


@app.route('/hypos/weight')
def weight_page():
    return render_template("hypo_weight.html", text="Weight")


@app.route('/hypos/bloodpressure')
def blood_pressure_page():
    return render_template("hypo_BP.html", text="Blood Pressure")


@app.route('/hypos/heartrate')
def heart_rate_page():
    return render_template("hypo_HR.html", text="Heart Rate")


@app.route('/hypos/sleepsummary')
def sleep_page():
    return render_template("hypo_sleep.html", text="Sleep")


if __name__ == "__main__":
    # logging.basicConfig(filename='logs/app.log', level=logging.DEBUG)
    logging.debug('Program started logging')
    set_local()
    app.secret_key = "buggs_bunny"
    # app.run(host='0.0.0.0', debug=True, port=5011)
    app.run()
